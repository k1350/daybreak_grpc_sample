#!/bin/sh
protoc --go_out=. --go_opt=paths=source_relative \
    --go-grpc_out=. --go-grpc_opt=paths=source_relative \
    proto/api/helloworld.proto

cp ./proto/api/* ./server/api
cp ./proto/api/* ./client/api