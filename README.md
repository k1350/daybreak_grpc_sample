# 動かし方
プロジェクトルートで
```
docker-compose build
docker-compose up
```

起動が終わったら別のターミナルを開いて
```
docker-compose exec server sh
```
で server 側のコンテナの中に入り、下記コマンドで server 側のコードを実行する。
```
go run /go/src/server/cmd/
```

別のターミナルを開いて
```
docker-compose exec client sh
```
で client 側のコンテナの中に入り、下記コマンドで client 側のコードを実行する。
```
go run /go/src/client/cmd/
```

client 側のコードを実行後、server 側のターミナルには

```
2021/04/25 05:34:51 Received: world
```

client 側のターミナルには

```
2021/04/25 05:34:51 Greeting: Hello world
```

と出るはず。

client 側のターミナルで
```
go run /go/src/client/cmd/ Alice
```

と実行すると、server 側のターミナルには

```
2021/04/25 05:34:57 Received: Alice
```

client 側のターミナルには
```
2021/04/25 05:34:57 Greeting: Hello Alice
```
と出るはず。